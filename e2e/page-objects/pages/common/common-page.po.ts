import { element, By } from '../../../../node_modules/protractor';

export class CommonPage {
    static get bookFieldName() {
        return element(By.name(`bn`));
    }
    static get bookFieldAuthor() {
        return element(By.name(`an`));
    }
    static get bookFieldIsbn() {
        return element(By.name(`isbn`));
    }
    static get bookButtonSave() {
        return element(By.xpath(`//button[@type='submit']`));
    }
}
