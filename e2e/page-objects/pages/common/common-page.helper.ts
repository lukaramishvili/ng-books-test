import { browser, ElementFinder, ExpectedConditions } from 'protractor';
import { CommonPage } from './common-page.po';
//import { CommonPageConstants } from './common-page.constants';
//import { resolve } from 'path';

export class CommonPageHelper {
    static async goTo(url: string){
        browser.waitForAngularEnabled();
        return browser.get(url);
    }
    static async fillBookFields(book: any) {
        await CommonPageHelper.fillInput(CommonPage.bookFieldName, book.name);
        await CommonPageHelper.fillInput(CommonPage.bookFieldAuthor, book.author);
        return CommonPageHelper.fillInput(CommonPage.bookFieldIsbn, book.author);
    }
    static async fillInput(input: ElementFinder, value: string){
        await browser.sleep(2000);
        return input.sendKeys(value);
    }
    static async clickButton(button: ElementFinder){
        await browser.sleep(2000);
        browser.wait(ExpectedConditions.elementToBeClickable(button));
        return button.click();
    }
    static async pressSaveButton() {
        CommonPageHelper.clickButton(CommonPage.bookButtonSave);
    }
}
