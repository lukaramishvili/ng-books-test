export class CommonPageConstants {
    static readonly subjectString = 'this is subject field';
    static readonly bodyString = 'this is body string';
    static readonly attachedFileName = 'mail.txt';
}
