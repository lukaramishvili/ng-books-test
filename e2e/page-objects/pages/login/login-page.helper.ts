import { CommonPageHelper } from '../common/common-page.helper';
import { LoginPage } from './login.po';
import { browser, ExpectedConditions } from '../../../../node_modules/protractor';

export class LoginPageHelper extends CommonPageHelper {
    static async goToPage() {
        return CommonPageHelper.goTo('/mail');
    }
    static async verifyLoginPageUserName() {
        await browser.wait(ExpectedConditions.visibilityOf(LoginPage.username));
    }
    static async enterUserName() {
        await LoginPage.username.sendKeys(browser.params.username);
        // provide username in protractor.conf.js
    }
    static async clickToUserNameNextButton() {
        await CommonPageHelper.click(LoginPage.userNameNextButton);
    }
    static async verifyLoginPagePassword() {
        await browser.wait(ExpectedConditions.visibilityOf(LoginPage.password));
    }
    static async enterPassword() {
        await LoginPage.password.sendKeys(browser.params.password);
        // provide username in protractor.conf.js
    }
    static async clickToPasswrdNextButton() {
        await LoginPage.passwordNextButton.click();
    }

}
