import {By, element} from 'protractor';

export class LoginPage {
    static get password() {
        return element(By.name('password'));
    }

    static get username() {
        return element(By.id('identifierId'));
    }

    static get passwordNextButton() {
        return element(By.xpath(`//div[@id='passwordNext']`));
    }
    static get userNameNextButton() {
     return element(By.id('identifierNext'));
    }
    static get composeButton() {
        return element(By.xpath(`//*[@role='button' and text()='Compose']`));
    }

}
