
// import { LoginPageHelper } from '../page-objects/pages/login/login-page.helper';
import { CommonPageHelper } from '../page-objects/pages/common/common-page.helper';
import { browser } from 'protractor';

fdescribe('Book suite', () => {
    
    beforeEach(async () => {
        // await LoginPageHelper.goToPage();
        // await LoginPageHelper.verifyLoginPageUserName();
        // await LoginPageHelper.enterUserName();
        // await LoginPageHelper.clickToUserNameNextButton();
        // await LoginPageHelper.verifyLoginPagePassword();
        // await LoginPageHelper.enterPassword();
        await browser.sleep(2000);
        // await LoginPageHelper.clickToPasswrdNextButton();
    });
    
    it('Add book', async () => {
        await CommonPageHelper.goTo("/");
        await CommonPageHelper.fillBookFields({ name: "კარლსონი", author: "ლინდგრენი", isbn: "23423452345" });
        await CommonPageHelper.pressSaveButton();
    });
});
